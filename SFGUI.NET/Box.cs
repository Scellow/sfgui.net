﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Box : Container
    {
        #region Enums
        public enum Orientation : byte
        {
            Horizontal = 0,
            Vertical = 1
        }
        #endregion

        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr BoxPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public float Spacing
        {
            get
            {
                EnsureValid();
                return GetSpacing(_pointer);
            }
            set
            {
                EnsureValid();
                SetSpacing(_pointer, value);
            }
        }
        #endregion

        #region Constructors
        public Box(Orientation Orientation = Orientation.Horizontal, float Spacing = 0f)
        {
            _pointer = CreateBox(Orientation, Spacing);
            base.Initialize(GetBaseContainer(_pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyBox(_pointer);
            base.Destroy();
        }
        public void Pack(Widget Widget, bool Expand = true, bool Fill = true)
        {
            EnsureValid();
            Pack(_pointer, Widget.WidgetPointer, Expand, Fill);
        }
        public void PackStart(Widget Widget, bool Expand = true, bool Fill = true)
        {
            EnsureValid();
            PackStart(_pointer, Widget.WidgetPointer, Expand, Fill);
        }
        public void PackEnd(Widget Widget, bool Expand = true, bool Fill = true)
        {
            EnsureValid();
            PackEnd(_pointer, Widget.WidgetPointer, Expand, Fill);
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Box) return IsEqual(_pointer, (Object as Box)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateBox(Orientation Orientation, float Spacing);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyBox(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_GetBaseContainer"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseContainer(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_Pack"), SuppressUnmanagedCodeSecurity]
        private static extern void Pack(IntPtr Pointer, IntPtr Widget, bool Expand, bool Fill);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_PackStart"), SuppressUnmanagedCodeSecurity]
        private static extern void PackStart(IntPtr Pointer, IntPtr Widget, bool Expand, bool Fill);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_PackEnd"), SuppressUnmanagedCodeSecurity]
        private static extern void PackEnd(IntPtr Pointer, IntPtr Widget, bool Expand, bool Fill);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_GetSpacing"), SuppressUnmanagedCodeSecurity]
        private static extern float GetSpacing(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBox_SetSpacing"), SuppressUnmanagedCodeSecurity]
        private static extern void SetSpacing(IntPtr Pointer, float Spacing);
        #endregion
    }
}
