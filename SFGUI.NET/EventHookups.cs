﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    internal class EventHookups
    {
        #region Variables
        private List<SFML.Window.Window> _eventsources = new List<SFML.Window.Window>();
        private Action<Event> _callback = null;
        #endregion

        #region Constructors
        public EventHookups(Action<Event> Callback)
        {
            _callback = Callback;
        }
        #endregion

        #region Functions
        public void AttachEvents(SFML.Window.Window Window)
        {
            if (_eventsources.Contains(Window)) return;
            _eventsources.Add(Window);
            Window.Closed += WindowClosed;
            Window.GainedFocus += WindowGainedFocus;
            Window.JoystickButtonPressed += WindowJoystickButtonPressed;
            Window.JoystickButtonReleased += WindowJoystickButtonReleased;
            Window.JoystickMoved += WindowJoystickMoved;
            Window.JoystickConnected += WindowJoystickConnected;
            Window.JoystickDisconnected += WindowJoystickDisconnected;
            Window.KeyPressed += WindowKeyPressed;
            Window.KeyReleased += WindowKeyReleased;
            Window.LostFocus += WindowLostFocus;
            Window.MouseButtonPressed += WindowMouseButtonPressed;
            Window.MouseButtonReleased += WindowMouseButtonReleased;
            Window.MouseEntered += WindowMouseEntered;
            Window.MouseLeft += WindowMouseLeft;
            Window.MouseMoved += WindowMouseMoved;
            Window.MouseWheelMoved += WindowMouseWheelMoved;
            Window.Resized += WindowResized;
            Window.TextEntered += WindowTextEntered;
        }
        public void DetachEvents(SFML.Window.Window Window)
        {
            _eventsources.Remove(Window);
            Window.Closed -= WindowClosed;
            Window.GainedFocus -= WindowGainedFocus;
            Window.JoystickButtonPressed -= WindowJoystickButtonPressed;
            Window.JoystickButtonReleased -= WindowJoystickButtonReleased;
            Window.JoystickMoved -= WindowJoystickMoved;
            Window.JoystickConnected -= WindowJoystickConnected;
            Window.JoystickDisconnected -= WindowJoystickDisconnected;
            Window.KeyPressed -= WindowKeyPressed;
            Window.KeyReleased -= WindowKeyReleased;
            Window.LostFocus -= WindowLostFocus;
            Window.MouseButtonPressed -= WindowMouseButtonPressed;
            Window.MouseButtonReleased -= WindowMouseButtonReleased;
            Window.MouseEntered -= WindowMouseEntered;
            Window.MouseLeft -= WindowMouseLeft;
            Window.MouseMoved -= WindowMouseMoved;
            Window.MouseWheelMoved -= WindowMouseWheelMoved;
            Window.Resized -= WindowResized;
            Window.TextEntered -= WindowTextEntered;
        }
        private void WindowClosed(object sender, EventArgs e)
        {
            _callback(new Event() { Type = EventType.Closed });
        }
        private void WindowGainedFocus(object sender, EventArgs e)
        {
            _callback(new Event() { Type = EventType.GainedFocus });
        }
        private void WindowJoystickButtonPressed(object sender, JoystickButtonEventArgs e)
        {
            _callback(new Event() { Type = EventType.JoystickButtonPressed, JoystickButton = new JoystickButtonEvent() { Button = e.Button, JoystickId = e.JoystickId } });
        }
        private void WindowJoystickButtonReleased(object sender, JoystickButtonEventArgs e)
        {
            _callback(new Event() { Type = EventType.JoystickButtonReleased, JoystickButton = new JoystickButtonEvent() { Button = e.Button, JoystickId = e.JoystickId } });
        }
        private void WindowJoystickMoved(object sender, JoystickMoveEventArgs e)
        {
            _callback(new Event() { Type = EventType.JoystickMoved, JoystickMove = new JoystickMoveEvent() { Axis = e.Axis, JoystickId = e.JoystickId, Position = e.Position } });
        }
        private void WindowJoystickConnected(object sender, JoystickConnectEventArgs e)
        {
            _callback(new Event() { Type = EventType.JoystickConnected, JoystickConnect = new JoystickConnectEvent() { JoystickId = e.JoystickId } });
        }
        private void WindowJoystickDisconnected(object sender, JoystickConnectEventArgs e)
        {
            _callback(new Event() { Type = EventType.JoystickDisconnected, JoystickConnect = new JoystickConnectEvent() { JoystickId = e.JoystickId } });
        }
        private void WindowKeyPressed(object sender, KeyEventArgs e)
        {
            _callback(new Event() { Type = EventType.KeyPressed, Key = new KeyEvent() { Alt = e.Alt ? 1 : 0, Code = e.Code, Control = e.Control ? 1 : 0, Shift = e.Shift ? 1 : 0, System = e.System ? 1 : 0 } });
        }
        private void WindowKeyReleased(object sender, KeyEventArgs e)
        {
            _callback(new Event() { Type = EventType.KeyReleased, Key = new KeyEvent() { Alt = e.Alt ? 1 : 0, Code = e.Code, Control = e.Control ? 1 : 0, Shift = e.Shift ? 1 : 0, System = e.System ? 1 : 0 } });
        }
        private void WindowLostFocus(object sender, EventArgs e)
        {
            _callback(new Event() { Type = EventType.LostFocus });
        }
        private void WindowMouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            _callback(new Event() { Type = EventType.MouseButtonPressed, MouseButton = new MouseButtonEvent() { Button = e.Button, X = e.X, Y = e.Y } });
        }
        private void WindowMouseButtonReleased(object sender, MouseButtonEventArgs e)
        {
            _callback(new Event() { Type = EventType.MouseButtonReleased, MouseButton = new MouseButtonEvent() { Button = e.Button, X = e.X, Y = e.Y } });
        }
        private void WindowMouseEntered(object sender, EventArgs e)
        {
            _callback(new Event() { Type = EventType.MouseEntered });
        }
        private void WindowMouseLeft(object sender, EventArgs e)
        {
            _callback(new Event() { Type = EventType.MouseLeft });
        }
        private void WindowMouseMoved(object sender, MouseMoveEventArgs e)
        {
            _callback(new Event() { Type = EventType.MouseMoved, MouseMove = new MouseMoveEvent() { X = e.X, Y = e.Y } });
        }
        private void WindowMouseWheelMoved(object sender, MouseWheelEventArgs e)
        {
            _callback(new Event() { Type = EventType.MouseWheelMoved, MouseWheel = new MouseWheelEvent() { Delta = e.Delta, X = e.X, Y = e.Y } });
        }
        private void WindowResized(object sender, SizeEventArgs e)
        {
            _callback(new Event() { Type = EventType.Resized, Size = new SizeEvent() { Width = e.Width, Height = e.Height } });
        }
        private void WindowTextEntered(object sender, TextEventArgs e)
        {
            _callback(new Event() { Type = EventType.TextEntered, Text = new TextEvent() { Unicode = (uint)Char.ConvertToUtf32(e.Unicode, 0) } });
        }
        #endregion
    }
}
