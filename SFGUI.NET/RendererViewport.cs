﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class RendererViewport : InternalBase
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr RendererViewportPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        #endregion

        #region Constructors
        internal RendererViewport(IntPtr Pointer)
        {
            _pointer = Pointer;
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyRendererViewport(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is RendererViewport) return IsEqual(_pointer, (Object as RendererViewport)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRendererViewport_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyRendererViewport(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRendererViewport_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);
        #endregion
    }
}
