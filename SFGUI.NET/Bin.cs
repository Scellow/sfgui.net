﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Bin : Container
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr BinPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public Widget Child
        {
            get
            {
                EnsureValid();
                var childptr = GetChild(_pointer);
                if (childptr != IntPtr.Zero) return new Widget(childptr);
                else return null;
            }
        }
        #endregion

        #region Constructors
        internal Bin() { }
        protected override void Initialize(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseContainer(Pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyBin(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Bin) return IsEqual(_pointer, (Object as Bin)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBin_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyBin(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBin_GetBaseContainer"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseContainer(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBin_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgBin_GetChild"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetChild(IntPtr Pointer);
        #endregion
    }
}
