﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SFGUINet
{
    internal class IEnumerableDebugView<T>
    {
        #region Variables
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private IEnumerable<T> _ienumerable;
        #endregion

        #region Properties
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items
        {
            get
            {
                return _ienumerable.ToArray();
            }
        }
        #endregion

        #region Constructors
        public IEnumerableDebugView(IEnumerable<T> IEnumerable)
        {
            _ienumerable = IEnumerable;
        }
        #endregion
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
    internal class CustomDebuggerDisplay : Attribute
    {
        #region Variables
        private string _format = "";
        #endregion

        #region Properties
        public string Format
        {
            get
            {
                return _format;
            }
        }
        #endregion

        #region Constructors
        public CustomDebuggerDisplay(string Format)
        {
            _format = Format;
        }
        #endregion

        #region Functions
        public static string Parse(string Format, object Target)
        {
            foreach (var match in Regex.Matches(Format, @"({[^}]*})"))
            {
                Format = Regex.Replace(Format, match.ToString(), GetValue(new List<string>(match.ToString().Substring(1, match.ToString().Length - 2).Split('.')), Target));
            }
            return Format;
        }
        private static string GetValue(List<string> Name, object Target)
        {
            string name = Name[0];
            Name.RemoveAt(0);
            try
            {
                var result = Target.GetType().GetProperty(name).GetValue(Target, null);
                if (Name.Count == 0) return result.ToString();
                else return GetValue(Name, Target);
            }
            catch { }
            try
            {
                var result = Target.GetType().GetField(name).GetValue(Target);
                if (Name.Count == 0) return result.ToString();
                else return GetValue(Name, Target);
            }
            catch { }
            return "[Error: Unable to find value]";
        }
        #endregion
    }

    internal class FlattenedDebugView
    {
        #region Variables
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private object _object = null;
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Item[] _values = null;
        #endregion

        #region Properties
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public Item[] Values
        {
            get
            {
                return _values;
            }
        }
        #endregion

        #region Constructors
        public FlattenedDebugView(object Object)
        {
            _object = Object;
            List<Item> values = new List<Item>();
            List<Item> staticvalues = new List<Item>();
            if (_object != null)
            {
                foreach (var field in _object.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy))
                {
                    if (ShouldSkip(field.GetCustomAttributes(true))) continue;
                    AddValue(field.IsStatic ? staticvalues : values, field.Name, field.GetValue(_object), field.FieldType.FullName);
                }
                foreach (var property in _object.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy))
                {
                    if (ShouldSkip(property.GetCustomAttributes(true))) continue;
                    object value = null;
                    bool isstatic = false;
                    try { if (property.CanRead) { value = property.GetValue(_object, null); isstatic = property.GetGetMethod().IsStatic; } else value = "[Error: Property has no getter]"; }
                    catch (Exception e) { value = e; }
                    AddValue(isstatic ? staticvalues : values, property.Name, value, property.PropertyType.FullName);
                }
            }
            if (staticvalues.Count > 0)
            {
                values.Add(new Item("Static Values", staticvalues, "", null));
            }
            _values = values.OrderBy(m => m.Name).ToArray();
        }
        #endregion

        #region Functions
        private bool ShouldSkip(object[] Attributes)
        {
            foreach (var attribute in Attributes)
            {
                if (attribute is DebuggerBrowsableAttribute && (attribute as DebuggerBrowsableAttribute).State == DebuggerBrowsableState.Never)
                {
                    return true;
                }
            }
            return false;
        }
        private void AddValue(List<Item> ValueList, string Name, object Value, string Type)
        {
            string customformat = null;
            if (Value != null)
            {
                foreach (var attribute in Value.GetType().GetCustomAttributes(typeof(CustomDebuggerDisplay), true))
                {
                    customformat = (attribute as CustomDebuggerDisplay).Format;
                    break;
                }
            }
            if (customformat == null) ValueList.Add(new Item(Name, Value, Type));
            else ValueList.Add(new Item(Name, Value, Type, CustomDebuggerDisplay.Parse(customformat, Value)));
        }
        #endregion

        #region Item Definition
        [DebuggerDisplay("{CustomValueDisplay,nq}", Name = "{Name,nq}", Type = "{Type,nq}")]
        public class Item
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string Name;
            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            public object Value;
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string Type;
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string CustomValueDisplay;

            public Item(string Name, object Value, string Type, string CustomValueDisplay = "")
            {
                this.Name = Name;
                this.Value = Value;
                this.Type = Type;
                if (CustomValueDisplay == null) this.CustomValueDisplay = "";
                else if (CustomValueDisplay.Length > 0) this.CustomValueDisplay = CustomValueDisplay;
                else if (Value != null) this.CustomValueDisplay = (Value is string) ? ('"' + Value.ToString() + '"') : Value.ToString();
            }
        }
        #endregion
    }
}
