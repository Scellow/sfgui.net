﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Label : Widget
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr LabelPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public bool LineWrap
        {
            get
            {
                EnsureValid();
                return GetLineWrap(_pointer);
            }
            set
            {
                EnsureValid();
                SetLineWrap(_pointer, value);
            }
        }
        public string Text
        {
            get
            {
                EnsureValid();
                return Marshal.PtrToStringAnsi(GetText(_pointer));
            }
            set
            {
                EnsureValid();
                SetText(_pointer, value);
            }
        }
        public string WrappedText
        {
            get
            {
                EnsureValid();
                return Marshal.PtrToStringAnsi(GetWrappedText(_pointer));
            }
        }
        #endregion

        #region Constructors
        public Label(string Text = "")
        {
            _pointer = CreateLabel(Text);
            base.Initialize(GetBaseWidget(_pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyLabel(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Label) return IsEqual(_pointer, (Object as Label)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateLabel(string Text);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyLabel(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_GetBaseWidget"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseWidget(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_GetLineWrap"), SuppressUnmanagedCodeSecurity]
        private static extern bool GetLineWrap(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_GetText"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetText(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_GetWrappedText"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetWrappedText(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_SetLineWrap"), SuppressUnmanagedCodeSecurity]
        private static extern void SetLineWrap(IntPtr Pointer, bool Wrap);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgLabel_SetText"), SuppressUnmanagedCodeSecurity]
        private static extern void SetText(IntPtr Pointer, string Text);
        #endregion
    }
}
