﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public partial class RadioButton
    {
        public class RadioButtonGroup : InternalBase
        {
            #region Variables
            private IntPtr _pointer = IntPtr.Zero;
            #endregion

            #region Properties
            internal IntPtr RadioButtonGroupPointer
            {
                get
                {
                    EnsureValid();
                    return _pointer;
                }
            }
            public NativeCollection<RadioButton> Members
            {
                get
                {
                    EnsureValid();
                    return new NativeCollection<RadioButton>(new GroupCollectionSource(this));
                }
            }
            #endregion

            #region Constructors
            public RadioButtonGroup()
            {
                _pointer = CreateRadioButtonGroup();
            }
            internal RadioButtonGroup(IntPtr Pointer)
            {
                _pointer = Pointer;
            }
            #endregion

            #region Functions
            protected override void Destroy()
            {
                DestroyRadioButtonGroup(_pointer);
                base.Destroy();
            }
            protected override bool IsEqual(InternalBase Object)
            {
                if (Object is RadioButtonGroup) return IsEqual(_pointer, (Object as RadioButtonGroup)._pointer);
                else return base.IsEqual(Object);
            }
            #endregion

            #region Native Source Impl
            private class GroupCollectionSource : NativeCollectionSource<RadioButton>
            {
                #region Variables
                private RadioButtonGroup _group = null;
                #endregion

                #region Constructors
                public GroupCollectionSource(RadioButtonGroup Group)
                {
                    _group = Group;
                }
                #endregion

                #region Functions
                public int GetItemCount()
                {
                    _group.EnsureValid();
                    return GetMemberCount(_group._pointer);
                }
                public RadioButton GetItemByIndex(int Index)
                {
                    _group.EnsureValid();
                    var radiobuttonptr = GetMemberByIndex(_group._pointer, Index);
                    if (radiobuttonptr != IntPtr.Zero) return new RadioButton(radiobuttonptr);
                    else return null;
                }
                #endregion
            }
            #endregion

            #region Imports
            [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButtonGroup_Create"), SuppressUnmanagedCodeSecurity]
            private static extern IntPtr CreateRadioButtonGroup();

            [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButtonGroup_Destroy"), SuppressUnmanagedCodeSecurity]
            private static extern void DestroyRadioButtonGroup(IntPtr Pointer);

            [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButtonGroup_IsEqual"), SuppressUnmanagedCodeSecurity]
            private static extern bool IsEqual(IntPtr A, IntPtr B);

            [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButtonGroup_GetMemberByIndex"), SuppressUnmanagedCodeSecurity]
            private static extern IntPtr GetMemberByIndex(IntPtr Pointer, int Index);

            [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButtonGroup_GetMemberCount"), SuppressUnmanagedCodeSecurity]
            private static extern int GetMemberCount(IntPtr Pointer);
            #endregion
        }
    }
}
