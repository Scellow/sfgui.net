﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Signal : InternalBase
    {
        #region Delegates
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void SignalCallback();
        #endregion

        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr SignalPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        #endregion

        #region Constructors
        internal Signal(IntPtr Pointer)
        {
            _pointer = Pointer;
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroySignal(_pointer);
            base.Destroy();
        }
        public uint Connect(SignalCallback Callback)
        {
            EnsureValid();
            return ConnectCallback(_pointer, Callback);
        }
        public void Disconnect(uint CallbackID)
        {
            EnsureValid();
            DisconnectCallback(_pointer, CallbackID);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSignal_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroySignal(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSignal_ConnectCallback"), SuppressUnmanagedCodeSecurity]
        private static extern uint ConnectCallback(IntPtr Pointer, SignalCallback Callback);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSignal_DisconnectCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void DisconnectCallback(IntPtr Pointer, uint CallbackID);
        #endregion
    }
}
