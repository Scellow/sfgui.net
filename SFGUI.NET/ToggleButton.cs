﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class ToggleButton : Button
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr ToggleButtonPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public bool Active
        {
            get
            {
                EnsureValid();
                return IsActive(_pointer);
            }
            set
            {
                EnsureValid();
                SetActive(_pointer, value);
            }
        }
        #endregion

        #region Signal ID(s)
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnToggle = new SignalID(SignalIDType.ToggleButton_OnToggle);
        #endregion

        #region Constructors
        internal ToggleButton() { }
        public ToggleButton(string Label)
        {
            _pointer = CreateToggleButton(Label);
            base.Initialize(GetBaseButton(_pointer));
        }
        protected override void Initialize(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseButton(Pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyToggleButton(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is ToggleButton) return IsEqual(_pointer, (Object as ToggleButton)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgToggleButton_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateToggleButton(string Label);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgToggleButton_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyToggleButton(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgToggleButton_GetBaseButton"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseButton(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgToggleButton_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgToggleButton_IsActive"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsActive(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgToggleButton_SetActive"), SuppressUnmanagedCodeSecurity]
        private static extern void SetActive(IntPtr Pointer, bool Active);
        #endregion
    }
}
