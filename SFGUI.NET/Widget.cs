﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Widget : Object
    {
        #region Enums
        public enum State : byte
        {
            Normal = 0,
            Active = 1,
            Prelight = 2,
            Selected = 3,
            Insensitive = 4
        }
        #endregion

        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr WidgetPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public Vector2f AbsolutePosition
        {
            get
            {
                EnsureValid();
                return GetAbsolutePosition(_pointer);
            }
        }
        public FloatRect Allocation
        {
            get
            {
                EnsureValid();
                return GetAllocation(_pointer);
            }
            set
            {
                EnsureValid();
                SetAllocation(_pointer, value);
            }
        }
        public string Name
        {
            get
            {
                EnsureValid();
                return Marshal.PtrToStringAnsi(GetName(_pointer));
            }
        }
        public string Class
        {
            get
            {
                EnsureValid();
                return Marshal.PtrToStringAnsi(GetClass(_pointer));
            }
            set
            {
                EnsureValid();
                SetClass(_pointer, value);
            }
        }
        public string Id
        {
            get
            {
                EnsureValid();
                return Marshal.PtrToStringAnsi(GetId(_pointer));
            }
            set
            {
                EnsureValid();
                SetId(_pointer, value);
            }
        }
        public int HierarchyLevel
        {
            get
            {
                EnsureValid();
                return GetHierarchyLevel(_pointer);
            }
            set
            {
                EnsureValid();
                SetHierarchyLevel(_pointer, value);
            }
        }
        public Container Parent
        {
            get
            {
                EnsureValid();
                return new Container(GetParent(_pointer));
            }
            set
            {
                EnsureValid();
                SetParent(_pointer, value.WidgetPointer);
            }
        }
        public Vector2f Requisition
        {
            get
            {
                EnsureValid();
                return GetRequisition(_pointer);
            }
            set
            {
                EnsureValid();
                SetRequisition(_pointer, value);
            }
        }
        public State WidgetState
        {
            get
            {
                EnsureValid();
                return GetState(_pointer);
            }
            set
            {
                EnsureValid();
                SetState(_pointer, value);
            }
        }
        public RendererViewport Viewport
        {
            get
            {
                EnsureValid();
                return new RendererViewport(GetViewport(_pointer));
            }
            set
            {
                EnsureValid();
                SetViewport(_pointer, value.RendererViewportPointer);
            }
        }
        public int ZOrder
        {
            get
            {
                EnsureValid();
                return GetZOrder(_pointer);
            }
            set
            {
                EnsureValid();
                SetZOrder(_pointer, value);
            }
        }
        public bool HasFocus
        {
            get
            {
                EnsureValid();
                return GotFocus(_pointer);
            }
        }
        public bool IsActiveWidget
        {
            get
            {
                EnsureValid();
                return ActiveWidget(_pointer);
            }
        }
        public bool IsGloballyVisible
        {
            get
            {
                EnsureValid();
                return GloballyVisible(_pointer);
            }
        }
        public bool IsLocallyVisible
        {
            get
            {
                EnsureValid();
                return LocallyVisible(_pointer);
            }
        }
        #endregion

        #region Signal ID(s)
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnStateChange = new SignalID(SignalIDType.Widget_OnStateChange);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnGainFocus = new SignalID(SignalIDType.Widget_OnGainFocus);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnLostFocus = new SignalID(SignalIDType.Widget_OnLostFocus);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnExpose = new SignalID(SignalIDType.Widget_OnExpose);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnSizeAllocate = new SignalID(SignalIDType.Widget_OnSizeAllocate);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnSizeRequest = new SignalID(SignalIDType.Widget_OnSizeRequest);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnMouseEnter = new SignalID(SignalIDType.Widget_OnMouseEnter);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnMouseLeave = new SignalID(SignalIDType.Widget_OnMouseLeave);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnMouseMove = new SignalID(SignalIDType.Widget_OnMouseMove);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnMouseLeftPress = new SignalID(SignalIDType.Widget_OnMouseLeftPress);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnMouseRightPress = new SignalID(SignalIDType.Widget_OnMouseRightPress);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnMouseLeftRelease = new SignalID(SignalIDType.Widget_OnMouseLeftRelease);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnMouseRightRelease = new SignalID(SignalIDType.Widget_OnMouseRightRelease);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnLeftClick = new SignalID(SignalIDType.Widget_OnLeftClick);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnRightClick = new SignalID(SignalIDType.Widget_OnRightClick);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnKeyPress = new SignalID(SignalIDType.Widget_OnKeyPress);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnKeyRelease = new SignalID(SignalIDType.Widget_OnKeyRelease);
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly SignalID OnText = new SignalID(SignalIDType.Widget_OnText);
        #endregion

        #region Constructors
        internal Widget() { }
        internal Widget(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseObject(Pointer));
        }
        protected override void Initialize(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseObject(Pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyWidget(_pointer);
            base.Destroy();
        }
        public void GrabFocus()
        {
            EnsureValid();
            GrabFocus(_pointer);
        }
        public void HandleAbsolutePositionChange()
        {
            EnsureValid();
            HandleAbsolutePositionChange(_pointer);
        }
        public void HandleEvent(Event Event)
        {
            EnsureValid();
            HandleEvent(_pointer, ref Event);
        }
        public void HandleGlobalVisibilityChange()
        {
            EnsureValid();
            HandleGlobalVisibilityChange(_pointer);
        }
        public void Invalidate()
        {
            EnsureValid();
            Invalidate(_pointer);
        }
        public void Refresh()
        {
            EnsureValid();
            Refresh(_pointer);
        }
        public void RequestResize()
        {
            EnsureValid();
            RequestResize(_pointer);
        }
        public void SetActiveWidget()
        {
            EnsureValid();
            SetActiveWidget(_pointer);
        }
        public void Show(bool ShowWidget = true)
        {
            EnsureValid();
            Show(_pointer, ShowWidget);
        }
        public void Update(float Seconds)
        {
            EnsureValid();
            Update(_pointer, Seconds);
        }
        public void UpdateDrawablePosition()
        {
            EnsureValid();
            UpdateDrawablePosition(_pointer);
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Widget) return IsEqual(_pointer, (Object as Widget)._pointer);
            else return base.IsEqual(Object);
        }
        public void SetPosition(Vector2f Position)
        {
            EnsureValid();
            SetPosition(_pointer, Position);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyWidget(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetAbsolutePosition"), SuppressUnmanagedCodeSecurity]
        private static extern Vector2f GetAbsolutePosition(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetAllocation"), SuppressUnmanagedCodeSecurity]
        private static extern FloatRect GetAllocation(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetName"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetName(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetClass"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetClass(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetId"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetId(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetHierarchyLevel"), SuppressUnmanagedCodeSecurity]
        private static extern int GetHierarchyLevel(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetParent"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetParent(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetRequisition"), SuppressUnmanagedCodeSecurity]
        private static extern Vector2f GetRequisition(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetState"), SuppressUnmanagedCodeSecurity]
        private static extern State GetState(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetViewport"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetViewport(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetZOrder"), SuppressUnmanagedCodeSecurity]
        private static extern int GetZOrder(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GrabFocus"), SuppressUnmanagedCodeSecurity]
        private static extern void GrabFocus(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_HandleAbsolutePositionChange"), SuppressUnmanagedCodeSecurity]
        private static extern void HandleAbsolutePositionChange(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_HandleEvent"), SuppressUnmanagedCodeSecurity]
        private static extern void HandleEvent(IntPtr Pointer, ref Event Event);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_HandleGlobalVisibilityChange"), SuppressUnmanagedCodeSecurity]
        private static extern void HandleGlobalVisibilityChange(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_HasFocus"), SuppressUnmanagedCodeSecurity]
        private static extern bool GotFocus(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_Invalidate"), SuppressUnmanagedCodeSecurity]
        private static extern void Invalidate(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_IsActiveWidget"), SuppressUnmanagedCodeSecurity]
        private static extern bool ActiveWidget(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_IsGloballyVisible"), SuppressUnmanagedCodeSecurity]
        private static extern bool GloballyVisible(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_IsLocallyVisible"), SuppressUnmanagedCodeSecurity]
        private static extern bool LocallyVisible(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_Refresh"), SuppressUnmanagedCodeSecurity]
        private static extern void Refresh(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_RequestResize"), SuppressUnmanagedCodeSecurity]
        private static extern void RequestResize(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetActiveWidget"), SuppressUnmanagedCodeSecurity]
        private static extern void SetActiveWidget(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetAllocation"), SuppressUnmanagedCodeSecurity]
        private static extern void SetAllocation(IntPtr Pointer, FloatRect Rect);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetClass"), SuppressUnmanagedCodeSecurity]
        private static extern void SetClass(IntPtr Pointer, string Class);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetHierarchyLevel"), SuppressUnmanagedCodeSecurity]
        private static extern void SetHierarchyLevel(IntPtr Pointer, int Level);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetId"), SuppressUnmanagedCodeSecurity]
        private static extern void SetId(IntPtr Pointer, string Class);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetParent"), SuppressUnmanagedCodeSecurity]
        private static extern void SetParent(IntPtr Pointer, IntPtr Parent);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetPosition"), SuppressUnmanagedCodeSecurity]
        private static extern void SetPosition(IntPtr Pointer, Vector2f Position);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetRequisition"), SuppressUnmanagedCodeSecurity]
        private static extern void SetRequisition(IntPtr Pointer, Vector2f Requisition);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetState"), SuppressUnmanagedCodeSecurity]
        private static extern void SetState(IntPtr Pointer, State State);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetViewport"), SuppressUnmanagedCodeSecurity]
        private static extern void SetViewport(IntPtr Pointer, IntPtr Viewport);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_SetZOrder"), SuppressUnmanagedCodeSecurity]
        private static extern void SetZOrder(IntPtr Pointer, int ZOrder);
        
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_Show"), SuppressUnmanagedCodeSecurity]
        private static extern void Show(IntPtr Pointer, bool Show);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_Update"), SuppressUnmanagedCodeSecurity]
        private static extern void Update(IntPtr Pointer, float Seconds);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_UpdateDrawablePosition"), SuppressUnmanagedCodeSecurity]
        private static extern void UpdateDrawablePosition(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_GetBaseObject"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseObject(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWidget_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);
        #endregion
    }
}
